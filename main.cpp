#include <iostream>
#include "IntArray.h"

using namespace std;

int main () {
    IntArray fir, sec(4), thi(5, 9), fou(thi);

    cout << "First array: default constructor: " << fir << endl;
    cout << "Second array: constructor by size: " << sec << endl;
    cout << "Third array: constructor by size and value: " << thi << endl;
    cout << "Fourth array:  copy constructor: " << sec << "\n" << endl;

    cout << "size of second array: " << sec.getSize() << endl;

cout << "redefine elements of sec: " << endl;
cin >> sec;
cout << "second element of " << sec << "is " << sec[2];

    thi.resize(3);
    cout << "reduce third array to 3-element: " << thi << endl;
    thi.resize(5);
    cout << "increase third array to 5-element: " << thi << endl;

    fir = thi;
    cout << "now first array is equal to third -- " << fir << "\n" << endl;

    cout << "first is equal to third? -- ";
    if (fir == thi)
        cout << "yes" << "\n" << fir << "\n" << thi << endl << "\n" ;
    else
        cout << "no" << "\n" << fir << "\n" << thi << endl << "\n" ;

    cout << "is first equal to fourth? -- ";
    if (fir == fou)
        cout << "yes" << "\n" << fir << "\n" << fou << endl << "\n" ;
    else
        cout << "no" << "\n" << fir << "\n" << fou << endl << "\n" ;

    try {
        cout << "is first equal to second? -- ";
        if (fir == sec)
            cout << "yes" << "\n" << fir << "\n" << sec << endl << "\n" ;
        else
            cout << "no" << "\n" << fir << "\n" << sec << endl << "\n" ;
    }
    catch (DynamicArrayException *e) {
        cout << e->getMessage() << "\n" << endl;
    }

    cout << "is first greater than second? -- ";
    if (fir > sec)
        cout << "yes" << "\n" << sec << "\n" << fir << endl << "\n" ;
    else
        cout << "no" << "\n" << sec << "\n" << fir << endl << "\n" ;


    cout << "is first greater or equal to third? -- ";
    if (fir >= thi)
        cout << "yes" << "\n" << fir << "\n" << thi << endl << "\n" ;
    else
        cout << "no" << "\n" << fir << "\n" << thi << endl << "\n" ;

    cout << "is first less than third? -- ";
    if (fir < thi)
        cout << "yes" << "\n" << fir << "\n" << thi << endl << "\n" ;
    else
        cout << "no" << "\n" << fir << "\n" << thi << endl << "\n" ;

    cout << "is first less or equal to second? -- ";
    if (fir <= sec)
        cout << "yes" << "\n" << sec << "\n" << fir << endl << "\n" ;
    else
        cout << "no" << "\n" << sec << "\n" << fir << endl << "\n" ;


    fou = fir + sec;
    cout << "Concatenation of first and third: " << fou << "\n" << endl;

    cout << "THAT'S ALL, FOLKS";

    return 0;
}