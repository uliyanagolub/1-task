//
// Created by E540 on 27/05/2020.
//

#ifndef INC_1_PAGE_INTARRAY_H
#define INC_1_PAGE_INTARRAY_H

#include <iostream>
#include "DynamicArrayException.h"

using namespace std;

class IntArray
{
    int capacity_;
    int reserve_;
    int* pointedArr_;

public:
    IntArray();
    explicit IntArray(int cap);
    IntArray(int cap, int init);
    IntArray(const IntArray &origin);
    IntArray(IntArray &&other);
    ~IntArray();

    int getSize();
    void resize(int newSize);

    int &operator[](int index)
    {
        return pointedArr_[index];
    }

    IntArray& operator= (const IntArray &other)
    {
        if(this==&other){return *this;}

        int* temp = new int [other.capacity_];

        for (int i = 0; i < other.capacity_; i++)
            temp[i] = other.pointedArr_[i];


        delete pointedArr_;
        pointedArr_ = temp;
        capacity_ = other.capacity_;

        return *this;
    }

    IntArray& operator= (IntArray&& other) noexcept {

        if(this==&other)
            return *this;

        delete pointedArr_;
        pointedArr_ = other.pointedArr_;
        capacity_ = other.capacity_;
        other.pointedArr_ = nullptr;

        return *this;
    }

    bool operator == (const IntArray &other) const{
        if (capacity_ != other.capacity_) {
            throw new DynamicArrayException("Exception: Different array length");

        }

        for (int i = 0; i < capacity_; i++){
            if ( pointedArr_[i] != other.pointedArr_[i])
                return false;
        }

        return true;
    }

    bool operator != (const IntArray &other) const
    {

        if (capacity_ != other.capacity_ )
            return true;

        for (int i = 0; i < capacity_; i++){
            if ( pointedArr_[i] != other.pointedArr_[i])
                return true;
        }

        return false;
    }

    bool operator < (const IntArray &other) const
    {
        for(int i = 0; i < min(capacity_, other.capacity_); i++)
        {
            if (pointedArr_[i] < other.pointedArr_[i])
                return true;
        }
        return false;
    }

    bool operator <= (const IntArray &other)
    {
        for(int i = 0; i < min(capacity_, other.capacity_); i++)
        {
            if (pointedArr_[i] > other.pointedArr_[i])
                return false;
        }
        return true;
    }

    bool operator > (const IntArray &other) const
    {
        for(int i = 0; i < min(capacity_, other.capacity_); i++)
        {
            if (pointedArr_[i] > other.pointedArr_[i])
                return true;
        }
        return false;
    }

    bool operator >= (const IntArray &other) const
    {
        for(int i = 0; i < min(capacity_, other.capacity_); i++)
        {
            if (pointedArr_[i] < other.pointedArr_[i])
                return false;
        }
        return true;
    }

    IntArray operator+(const IntArray& other) {
        IntArray summ(capacity_+other.capacity_);

        for (int i = 0; i < capacity_; i++)
            summ.pointedArr_[i] = pointedArr_[i];
        for (int i = capacity_; i < capacity_ + other.capacity_; i++)
            summ[i] = other.pointedArr_[i - capacity_];

        return summ;
    }


    friend ostream &operator<<(std::ostream &os, const IntArray &arr);
    friend istream &operator>>(std::istream &in, IntArray &arr);

};







#endif //INC_1_PAGE_INTARRAY_H
