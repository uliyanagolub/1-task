//
// Created by E540 on 30/05/2020.
//

#ifndef INC_1_PAGE_DYNAMICARRAYEXCEPTION_H
#define INC_1_PAGE_DYNAMICARRAYEXCEPTION_H

using namespace std;

class DynamicArrayException {
private:
    string message;
public:
    DynamicArrayException(string message){
        this->message = message;
    }

    string getMessage(){
        return message;
    }
};

#endif //INC_1_PAGE_DYNAMICARRAYEXCEPTION_H
