//
// Created by E540 on 03/06/2020.
//
#include <iostream>
#include "IntArray.h"

IntArray::IntArray()
{
    capacity_ = 0;
    reserve_ = 0;
    pointedArr_ = new int[capacity_];
}

IntArray::IntArray(int cap)
{
    capacity_ = cap;
    reserve_ = 0;
    pointedArr_ = new int[capacity_];

    for (int i = 0; i < capacity_; i++)
        pointedArr_[i] = 0;
}

IntArray::IntArray(int cap, int init)
{
    capacity_ = cap;
    reserve_ = 0;
    pointedArr_ = new int[capacity_];

    for (int i = 0; i < capacity_; i++)
        pointedArr_[i] = init;

}

IntArray::IntArray(const IntArray &origin) //copy
{
    capacity_ = origin.capacity_;
    reserve_ = origin.reserve_;
    pointedArr_ = new int [capacity_];

    for (int i = 0; i < capacity_; i++)
        pointedArr_[i] = origin.pointedArr_[i];


}

IntArray::IntArray(IntArray &&other)
        :pointedArr_(other.pointedArr_), capacity_(other.capacity_), reserve_(other.reserve_)
{
    other.pointedArr_ = nullptr;
}

IntArray::~IntArray()
{
    delete[]pointedArr_;
}

int IntArray::getSize() { return capacity_; };

void IntArray::resize(int newSize)
{
    int* newArr = new int [newSize];

    if (newSize <= capacity_){
        for (int i = 0; i < newSize; i++)    //переместила в новый массив
            newArr[i] = pointedArr_[i];

        delete(pointedArr_);                //удалила старый
        pointedArr_ = newArr;               //переместила указатель
        capacity_ = newSize;
    }

    else{
        for (int i = 0; i < capacity_; i++)
            newArr[i] = pointedArr_[i];

        for (int i = capacity_; i < newSize; i++)
            newArr[i] = 0;

        delete pointedArr_;
        pointedArr_ = newArr;
        capacity_ = newSize;
    }
}

ostream &operator<<(std::ostream &os, const IntArray &arr)
{
    for (int i = 0; i < arr.capacity_; i++)
        os << arr.pointedArr_ [i] << " ";

    return os;
}

istream &operator>>(std::istream &in, IntArray &arr)
{
    std::cout << "Enter array" << endl;
    for (int i = 0; i < arr.capacity_; i++)
        cin >> arr.pointedArr_[i];

}